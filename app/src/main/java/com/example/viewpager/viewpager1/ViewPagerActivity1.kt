package com.example.viewpager.viewpager1

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.example.viewpager.R
import kotlinx.android.synthetic.main.activity_viewpager1.*

class ViewPagerActivity1 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_viewpager1)

        val adapter = MyPagerAdapter()
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                Log.d("App", "onPageScrollStateChanged: $state")
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                Log.d("App", "onPageScrolled: position=$position, " +
                        "positionOffset=$positionOffset, " +
                        "positionOffsetPixels=$positionOffsetPixels")
            }

            override fun onPageSelected(position: Int) {
                Log.d("App", "onPageSelected: $position")
                Toast.makeText(this@ViewPagerActivity1, "onPageSelected: $position", Toast.LENGTH_SHORT).show()
            }
        })
        viewPager.currentItem = 0
    }

    override fun onDestroy() {
        viewPager.clearOnPageChangeListeners()
        super.onDestroy()
    }
}