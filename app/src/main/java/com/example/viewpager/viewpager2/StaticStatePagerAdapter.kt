package com.example.viewpager.viewpager2

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class StaticStatePagerAdapter(fm: FragmentManager)
    : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val fragments = mutableListOf<Fragment>()

    init {
        fragments.add(MyFragment.newInstance(0))
        fragments.add(MyFragment.newInstance(1))
        fragments.add(MyFragment.newInstance(2))
        fragments.add(MyFragment.newInstance(3))
        fragments.add(MyFragment.newInstance(4))
        fragments.add(MyFragment.newInstance(5))
        fragments.add(MyFragment.newInstance(6))
        fragments.add(MyFragment.newInstance(7))
    }

    override fun getItem(position: Int): Fragment =
        fragments[position]

    override fun getCount(): Int =
        fragments.size

    override fun getPageTitle(position: Int): CharSequence =
        "Title $position"
}