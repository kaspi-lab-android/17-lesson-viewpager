package com.example.viewpager.viewpager2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.viewpager.R
import kotlinx.android.synthetic.main.activity_viewpager2.*

class ViewPagerActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_viewpager2)

        val adapter = StaticStatePagerAdapter(supportFragmentManager)
        viewPager.adapter = adapter
        // offscreen page limit
        viewPager.offscreenPageLimit = 1
        // attach tablayout to viewpager
        tabLayout.setupWithViewPager(viewPager)
    }
}