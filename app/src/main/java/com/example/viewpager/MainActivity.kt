package com.example.viewpager

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.viewpager.viewpager1.ViewPagerActivity1
import com.example.viewpager.viewpager2.ViewPagerActivity2
import com.example.viewpager.viewpager3.ViewPagerActivity3
import com.example.viewpager.viewpager4.ViewPagerActivity4
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewPager1.setOnClickListener { startActivity(Intent(this, ViewPagerActivity1::class.java)) }
        viewPager2.setOnClickListener { startActivity(Intent(this, ViewPagerActivity2::class.java)) }
        viewPager3.setOnClickListener { startActivity(Intent(this, ViewPagerActivity3::class.java)) }
        viewPager4.setOnClickListener { startActivity(Intent(this, ViewPagerActivity4::class.java)) }
    }
}