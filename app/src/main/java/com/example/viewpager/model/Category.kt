package com.example.viewpager.model

data class Category(
    val id: Int,
    val title: String
)