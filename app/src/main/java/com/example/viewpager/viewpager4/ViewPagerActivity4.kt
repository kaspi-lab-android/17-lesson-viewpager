package com.example.viewpager.viewpager4

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.viewpager.R
import com.example.viewpager.util.Store
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_viewpager4.*

class ViewPagerActivity4 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_viewpager4)

        val adapter = CategoryAdapter()
        adapter.setCategories(Store.getCategories())
        viewPager.adapter = adapter
        viewPager.setCurrentItem(0, true)

        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = adapter.getTitle(position)
        }.attach()

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                Toast.makeText(this@ViewPagerActivity4, "onTabSelected: ${tab.position}", Toast.LENGTH_SHORT).show()
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {
                Toast.makeText(this@ViewPagerActivity4, "onTabReselected: ${tab.position}", Toast.LENGTH_SHORT).show()
            }
        })
    }
}