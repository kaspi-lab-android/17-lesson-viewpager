package com.example.viewpager.viewpager4

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.viewpager.R
import com.example.viewpager.model.Category
import kotlinx.android.synthetic.main.fragment2.view.*

class CategoryViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

    constructor(parent: ViewGroup)
            : this(LayoutInflater.from(parent.context).inflate(R.layout.fragment2, parent, false))

    fun bind(category: Category) {
        itemView.textView.text = category.title
    }
}