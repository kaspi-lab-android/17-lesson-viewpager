package com.example.viewpager.viewpager4

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.viewpager.model.Category

class CategoryAdapter : RecyclerView.Adapter<CategoryViewHolder>() {
    private val list = mutableListOf<Category>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder =
        CategoryViewHolder(parent)

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int =
        list.size

    fun getTitle(position: Int): String =
        list[position].title

    fun setCategories(list: List<Category>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }
}