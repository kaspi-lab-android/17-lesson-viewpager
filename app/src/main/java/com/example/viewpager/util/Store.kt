package com.example.viewpager.util

import com.example.viewpager.model.Category

object Store {

    fun getCategories(): List<Category> =
        listOf(
            Category(0, "Все"),
            Category(1, "Обувь"),
            Category(2, "Одежда"),
            Category(3, "Мужчины"),
            Category(4, "Женщины"),
            Category(5, "Подростки"),
            Category(6, "Дети"),
            Category(7, "Скидки")
        )
}