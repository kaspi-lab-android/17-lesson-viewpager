package com.example.viewpager.viewpager3

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.viewpager.model.Category

class DynamicStatePagerAdapter(activity: FragmentActivity)
    : FragmentStateAdapter(activity) {

    private var categoryFragmentHolders = listOf<CategoryHolder>()

    override fun createFragment(position: Int): Fragment =
        categoryFragmentHolders[position].fragment

    override fun containsItem(itemId: Long): Boolean =
        categoryFragmentHolders.firstOrNull { it.category.id.toLong() == itemId } != null

    override fun getItemId(position: Int): Long =
        categoryFragmentHolders[position].category.id.toLong()

    override fun getItemCount(): Int =
        categoryFragmentHolders.size

    fun setCategories(categories: List<Category>) {
        if (categories != categoryFragmentHolders.map { it.category }) {
            categoryFragmentHolders = categories.map {
                val fragment = CategoryFragment.newInstance(it.id, it.title)
                CategoryHolder(fragment, it)
            }
            notifyDataSetChanged()
        }
    }

    private data class CategoryHolder(val fragment: Fragment, val category: Category)
}