package com.example.viewpager.viewpager3

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.viewpager.R
import com.example.viewpager.util.Store
import kotlinx.android.synthetic.main.activity_viewpager3.*
import kotlinx.android.synthetic.main.activity_viewpager3.viewPager

class ViewPagerActivity3 : AppCompatActivity() {

    private val pageChangeCallback = object: ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            Toast.makeText(this@ViewPagerActivity3, "onPageSelected: $position", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_viewpager3)
        viewPager.registerOnPageChangeCallback(pageChangeCallback)

        val adapter = DynamicStatePagerAdapter(this)
        adapter.setCategories(Store.getCategories())
        viewPager.orientation = ViewPager2.ORIENTATION_VERTICAL
        viewPager.adapter = adapter
        indicator.attachToPager(viewPager)
        viewPager.setCurrentItem(1, false)
    }

    override fun onDestroy() {
        viewPager.unregisterOnPageChangeCallback(pageChangeCallback)
        super.onDestroy()
    }
}