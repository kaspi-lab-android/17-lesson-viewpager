package com.example.viewpager.viewpager3

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.viewpager.R
import kotlinx.android.synthetic.main.fragment1.*

class CategoryFragment : Fragment(R.layout.fragment2) {

    private var categoryId = 0
    private var categoryName = ""

    companion object {

        private const val EXTRA_CATEGORY_ID = "EXTRA_CATEGORY_ID"
        private const val EXTRA_CATEGORY_NAME = "EXTRA_CATEGORY_NAME"

        fun newInstance(categoryId: Int, categoryName: String): CategoryFragment {
            val fragment = CategoryFragment()
            val bundle = Bundle()
            bundle.putInt(EXTRA_CATEGORY_ID, categoryId)
            bundle.putString(EXTRA_CATEGORY_NAME, categoryName)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        categoryId = arguments?.getInt(EXTRA_CATEGORY_ID, categoryId) ?: categoryId
        categoryName = arguments?.getString(EXTRA_CATEGORY_NAME, categoryName) ?: categoryName

        textView.text = "$categoryId: $categoryName"
    }
}